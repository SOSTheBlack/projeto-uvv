#define _CRT_SECURE_NO_WARNINGS
#define Vet_Linhas 4 //produto

#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <locale.h> // Biblioteca para setar caracteres es
//#include <conio.c> // Biblioteca para funções de mudar cor e fundo do texto


int main() {

    setlocale(LC_ALL, "Portuguese");
    int i, j, aux;
    int option = 0;
    char voltar_menu;
    char letter;
    int input = 0;
    int erro_log = 0;
    int total_estoque;

    // Colunas do vetor estoque
    // a- Código
    // b- Preço de custo
    // c- Preço de venda
    // d- Quantidade em estoque
    // e- Quantidade vendida
    int estoque_codigo[Vet_Linhas];
    float estoque_custo[Vet_Linhas];
    float estoque_preco[Vet_Linhas];
    int estoque_qtd[Vet_Linhas];
    int estoque_vendidos[Vet_Linhas];

    int estoque_codigo_aux;
    float estoque_custo_aux;
    float estoque_preco_aux;
    int estoque_qtd_aux;
    int estoque_vendidos_aux;

    // Inicia a matriz_estoque com colunas zeradas. (0)
    // Loop para Zerar o estoque, e cadastrar 4 exemplos.
    for(i = 0; i < Vet_Linhas; i++) {
        estoque_codigo[i] = 0;
        estoque_custo[i] = 0;
        estoque_preco[i] = 0;
        estoque_qtd[i] = 0;
        estoque_vendidos[i] = 0;
    }

    // pesquisa sequencial
    int consultar(int comparador){
        int i=0;
        for(i = 0; i < Vet_Linhas ; i++){

            if(estoque_codigo[i] == comparador){
                return i;
                break;
            }
        }
        return -1;
    }

    void bolhaFloat(float *vetor){
        int x, y;
        float aux;
        for(x=0; x<Vet_Linhas; x++){
            for(y= x+1; y<Vet_Linhas; y++){ // A Contagem de Y, está uma posição à frente da contagem de X
                if(vetor[x] > vetor[y]){ // Se o Valor do Vetor[x] for maior que o valor do Vetor[y], faremos a re-ordenação.
                    aux = vetor[x]; // aux guarda o valor menor
                    vetor[x] = vetor[y]; // Pega o valor ALTO e coloca no lugar do valor BAIXO
                    vetor[y] = aux; // Capturamos o valor ALTO guardado em aux, e re-colocamos em uma posição a frente do valor baixo
                }
            }
        }
    }

    void bolhaInt(int *vetor){
        int x,y, aux;
        for(x=0; x<Vet_Linhas; x++){
            for(y= x+1; y<Vet_Linhas; y++){ // A Contagem de Y, está uma posição à frente da contagem de X
                if(vetor[x] > vetor[y]){ // Se o Valor do Vetor[x] for maior que o valor do Vetor[y], faremos a re-ordenação.
                    aux = vetor[x]; // aux guarda o valor menor
                    vetor[x] = vetor[y]; // Pega o valor ALTO e coloca no lugar do valor BAIXO
                    vetor[y] = aux; // Capturamos o valor ALTO guardado em aux, e re-colocamos em uma posição a frente do valor baixo
                }
            }
        }
    }

    // Função para informar a quantidade de produtos cadastrados.
    int contarEstoque(int *vetor){
        int i = 0;
        int j = 0;
        for(i=0; i <= Vet_Linhas; i++){
            if(vetor[i] != 0){
                j++;
            }
            return j;
        }
    }

    void inserir(void) {
        // O Laço irá percorrer toda matriz, até encontrar um índice cujo valor seja igual à 0.
        // quando encontrar o valor igual a zero, ele irá parar e aquele índice estará com as novas informações.
        int i, x, y;

        total_estoque = contarEstoque(estoque_codigo);

        for(i=0; i<Vet_Linhas; i++){

            if(estoque_codigo[i] == 0) {
                if (total_estoque <= Vet_Linhas){
                    estoque_codigo[i] = estoque_codigo_aux;
                    estoque_custo[i] = estoque_custo_aux;
                    estoque_preco[i] = estoque_preco_aux;
                    estoque_qtd[i] = estoque_qtd_aux;
                    estoque_vendidos[i] = 0;

                    printf("\n\n\n");
                    printf("===================== PRODUTO CADASTRADO COM SUCESSO  ======================\n");
                    printf("     Produto: Cod:%d | Custo: %.3f | Preço de Venda: %.3f | Qtd Estoque: %d     \n", estoque_codigo[i], estoque_custo[i], estoque_preco[i], estoque_qtd[i]);
                    printf("============================================================================\n\n");

                break;
                }

            }

            if(total_estoque <= Vet_Linhas){
                printf("\n Você só pode cadastrar %d Produtos!", Vet_Linhas);
                break;
            }
        }

        bolhaInt(estoque_codigo);
        bolhaFloat(estoque_custo);
        bolhaFloat(estoque_preco);
        bolhaInt(estoque_qtd);
        bolhaInt(estoque_vendidos);
    };

    int excluir(int posicao){
        estoque_codigo[posicao] = 0;
        estoque_custo[posicao] = 0;
        estoque_preco[posicao] = 0;
        estoque_qtd[posicao] = 0;
        estoque_vendidos[posicao] = 0;

        total_estoque = contarEstoque(estoque_codigo);

        return 1;
    };

    void lucro_liquido(void){
        float lucro_liquido = 0;

        float x, y;
        for (i = 0; i < Vet_Linhas; ++i) {
            if(estoque_codigo[i] != 0){
                x = estoque_preco[i] - estoque_custo[i];
                y = estoque_qtd[i] * x;
            }
            lucro_liquido = y + y;
        }

        printf("\n Lucro Liquido Total = %.3f", lucro_liquido);
    }

    do {

        system("@cls||clear");
        printf("=============================================================================\n");
        printf("=                      CONTROLE DE ESTOQUE E VENDAS                          =\n");
        printf("=============================================================================\n\n");
        printf("Menu:\n");
        printf("    1. Inserir  produto no estoque\n");
        printf("    2. Excluir  produto do estoque\n");
        printf("    3. Realizar venda\n");
        printf("    4. Procurar um produto\n");
        printf("    5. Emitir relatório gerencial\n");
        printf("    6. Finalizar\n");

        printf("\n Entre com o número da opção desejada: ");
        scanf("%d", &option);
        system("@cls||clear");

    switch (option){
        case 1: { // Inserir Produto no Banco de Dados

            printf("=============================================================================\n");
            printf("=        CADASTRANDO NOVO PRODUTO          %d Produtos Cadastrados          =\n", total_estoque);
            printf("=============================================================================\n\n");

            do {
                erro_log = 0;
                printf("\n Digite o código do produto:");
                scanf("%d", &estoque_codigo_aux);

                if(consultar(estoque_codigo_aux) != -1) {
                    printf("\n ! O Código (%d) que você digitou, já existe em nosso banco de dados!\n", estoque_codigo_aux);
                    erro_log = 1;
                }

            } while(erro_log != 0);

            do {
                erro_log = 0;
                printf("\n Digite o preço de custo do produto: R$");
                scanf("%f", &estoque_custo_aux);

                printf("\n Informe o preço de venda do produto: R$ ");
                scanf("%f", &estoque_preco_aux);

                // Condição, Se O Valor de venda for menor que o preço de custo. Exibir alerta.
                if(estoque_preco_aux < estoque_custo_aux){
                    printf("\n   Atenção ! \n   Preço de venda (R$ %.3f) é menor que preço de custo (R$ %.3f), você terá prejuizo\n  Deseja Continuar com este valor? [S/N]: ", estoque_preco_aux, estoque_custo_aux);
                    scanf(" %c", &letter);
                    if (letter == 'n' || letter == 'N'){
                        erro_log = 1;
                    }
                }
            } while (erro_log != 0);


            printf("\n Informe a quantidade do produto:");
            scanf("%d", &estoque_qtd_aux);
            //
            estoque_vendidos_aux = 0;

            //Se a variavel erro_log não existir registro de erro, executar a função para inserir o produto no vetor.
            if(erro_log == 0){
                inserir();
            }

            break;
        }
        case 2: { // Excluir Produto do Estoque

                total_estoque = contarEstoque(estoque_codigo);

                printf("=========================== %d PRODUTOS CADASTRADOS ===========================\n", total_estoque);
                for(i=0; i<Vet_Linhas; i++){
                    if(estoque_codigo[i] != 0){
                        printf("     Produto: Cod:%d | Custo: %.3f | Preço de Venda: %.3f | Qtd Estoque: %d \n", estoque_codigo[i], estoque_custo[i], estoque_preco[i], estoque_qtd[i]);
                    }
                }
                printf("============================================================================\n\n");

                if(total_estoque != 0){

                    do {
                        // Iniciamos o controle de erro.
                        erro_log = 0;

                        // Perguntamos o código do produto a ser excluido, e consultamos para ter certeza que ele existe.
                        printf("\n\n Informe o código do produto que deseja excluir: ");
                        scanf("%d", &input);

                        aux = consultar(input); // a função consultar vai retornar a posição do vetor, em que a informação foi encontrada no vetor.
                        // se o código informado, existir. faremos a exclusão.
                        if(aux != -1){
                            // Abaixo vamos remover a informação do vetor informado pela função consultar.
                            printf("\n Produto Excluido com Sucesso!");
                            excluir(aux);
                            contarEstoque(estoque_codigo);
                        }
                        else { //  Se não, informamos que não existe e registramos um erro.
                            printf("\n O Código que você informou, não existe em nosso banco de dados, informe outro código !");
                        }

                    } while(erro_log != 0);
                }
                else {
                    printf("\n Atualmente não existe nenhum produto cadastrado no banco de dados!");
                }

                break;
        }
        case 3: { //Realizar Venda

                total_estoque = contarEstoque(estoque_codigo);

                printf("=============================================================================\n");
                printf("=                               REALIZANDO VENDA                            =\n");
                printf("=============================================================================\n\n");

                printf("=========================== %d PRODUTOS CADASTRADOS ===========================\n", total_estoque);
                for(i=0; i<Vet_Linhas; i++){
                    if(estoque_codigo[i] != 0){
                        printf("     Produto: Cod:%d | Custo: %.3f | Preço de Venda: %.3f | Qtd Estoque: %d \n", estoque_codigo[i], estoque_custo[i], estoque_preco[i], estoque_qtd[i]);
                    }
                }
                printf("============================================================================\n\n");


                if(total_estoque != 0){
                    do{
                        erro_log = 0;
                        printf("\n Informe o código do produto para realizar a venda: ");
                        scanf("%d", &input);
                        aux = consultar(input);
                        printf("\n Posição do ponteiro: %d", aux);

                        if(aux != -1){

                            printf("\n Informe a quantidade de produtos vendidos: ");
                            scanf("%d", &estoque_qtd_aux);

                            if(estoque_qtd_aux > estoque_qtd[aux]) {
                                printf("\n Infelizmente não temos estoque para %d vendas deste produto, apenas %d. \n", estoque_qtd_aux, estoque_qtd[aux]);
                                erro_log = 1;
                            }
                            else {
                                estoque_qtd[aux] = estoque_qtd[aux] - estoque_qtd_aux;
                                estoque_vendidos[aux] = estoque_qtd_aux;

                                printf("\n Venda cadastrada com sucesso.");
                                printf("\n Itens Vendidos: %d", estoque_qtd_aux);
                                printf("\n Valor total da venda: %f", estoque_preco[aux] * estoque_qtd_aux);
                            }
                        }
                        else {
                            printf("\n Código informado não existe!");
                        }

                    } while(erro_log != 0);
                }
                else { //  Se não, informamos que não existe e registramos um erro.
                    printf("\n O Código que você informou, não existe em nosso banco de dados, informe outro código !");
                }
                break;
        }
        case 4: { // Procurar produto(s)

            total_estoque = contarEstoque(estoque_codigo);
            printf("=============================================================================\n");
            printf("=           PESQUISANDO PRODUTOS NO ESTOQUE = TOTAL (X) PRODUTOS             =\n");
            printf("============================================================================\n\n");

            do {

                erro_log = 0;
                printf("\n Informe o código do produto.");
                scanf("%d", &input);

                aux = consultar(input);
                if(aux != -1){
                    printf("     Produto: Cod:%d | Custo: %.3f | Preço de Venda: %.3f | Qtd Estoque: %d \n", estoque_codigo[aux], estoque_custo[aux], estoque_preco[aux], estoque_qtd[aux]);
                }
                else{
                    printf("\n  O Código que você informou, não foi encontrado, tente novamente com outro código!");
                }

            } while(erro_log != 0);

            break;
        }
        case 5: { // Emitir Relatório Gerencial
            // Total de Produtos Cadastrados:
            // Total de Vendas
            // Dinheiro em Caixa
            // Lucro total das vendas

            printf("=============================================================================\n");
            printf("=                   RELATÓRIO GERENCIAL DA EMPRESA XYZ                       =\n");
            printf("============================================================================\n\n");

            lucro_liquido();

            break;
        }
        default:{
            printf("  Opção inválida.");
            system("@cls||clear");
            break;
        }

    }

    if (option != 6){
        printf("\n\nDeseja voltar para o menu? [S/N] :");
        scanf(" %c", &letter);

        if (letter == 's' || letter == 'S')
        {
            system("@cls||clear");
        }
        else
        {
            break;
        }
    }
    } while (option != 6);

    return 0;

}// FECHA MAIN
